'use strict'
// --!-- function def =============================


// --!-- init plugins =============================
$(document).ready(function () {
    // loader
    $(window).on('load', function () {
        $('#preloader').fadeOut(function () {
            $(this).remove();
        });
        $('body').removeClass('no-scroll');
    });
    setTimeout(function () {
        if ($("#preloader").length) {
            $("#preloader").fadeOut(function () {
                $(this).remove();
            });
        }
        $('body').removeClass('no-scroll');
    }, 1000);
    // imgLiquid init
    $(".img-fill").imgLiquid({
        fill: true,
        horizontalAlign: "center",
        verticalAlign: "center"
    });
    $(".img-no-fill").imgLiquid({
        fill: false,
        horizontalAlign: "center",
        verticalAlign: "center"
    });


    // pages logic ==================================
    $('.text-filer input').on('input', function () {
        var btn = $(this).parent().find('button');
        if ($(this).val() != '') {
            btn.show();
        }
        else {
            btn.hide();
        }
    });
    $('.text-filer button').on('click', function () {
        var inpt = $(this).parent().find('input');
        inpt.val('');
        inpt.trigger('keyup');
        $(this).hide();
    });
    if ($(".table-responsive").length) {
        const psEl = document.querySelectorAll('.table-responsive');
        for (var i = 0; i < psEl.length; i++) {
            const ps = new PerfectScrollbar(psEl[i], {
                wheelPropagation: true,
                swipeEasing: false
            });
            setTimeout(function () {
                ps.update();
            }, 50);
        }
    }

    if ($(".index-page").length) {
        var dynamicMask = new IMask(
            document.getElementById('search-card-input'),
            {
                mask: '000 00000 00000000000',
                lazy: false,
                placeholderChar: '-',
            }
        );
    }

    if ($(".search-page").length) {
        var dynamicMask = new IMask(
            document.getElementById('search-card-input'),
            {
                mask: '000 00000 00000000000',
                lazy: false,
                placeholderChar: '-',
            }
        );
    }

    if ($('.order_list-page').length) {
        $('.input-group.date').datepicker({
            format: "dd.mm.yyyy",
            todayBtn: "linked",
            clearBtn: true,
            language: "ru",
            autoclose: true
        }).on('changeDate', function (e) {

        });
        $('.selectpicker').selectpicker({});
        $('[data-toggle="popover"]').popover({
            container: 'body',
            html: true,
            content: function () {
                var url = $(this).data('full');
                return '<img class="popover-img" src="' + url + '">'
            }
        });

        // delete tr
        $('.table-block').on('click', '.btn-del', function (e) {
            $('#deleteModal .modal-title').html('Вы действительно хотите <br> удалить заказ № ' + $(this).parents('tr').find('.data-name').text() + '?');
        });

    }

    if ($('.order_detail-page').length) {
        $('.input-group.date').datepicker({
            format: "dd.mm.yyyy",
            todayBtn: "linked",
            clearBtn: true,
            language: "ru",
            autoclose: true
        }).on('changeDate', function (e) {
        });

    }

    if ($('.operations_list-page').length) {
        $('.input-group.date').datepicker({
            format: "dd.mm.yyyy",
            todayBtn: "linked",
            clearBtn: true,
            language: "ru",
            autoclose: true
        }).on('changeDate', function (e) {
        });
    }

    if ($('.operations_detail-page').length) {

    }

    if ($('.new_order-page').length) {
        $('.selectpicker').selectpicker({});
    }

    if ($(".new_operations-page").length) {
        $('.file-filter input').on('change', function () {
            let filename = $(this).val().split('\\').pop();
            if (filename)
                $(".file-filter .custom-file-label").text(filename);
            else
                $(".file-filter .custom-file-label").text('Выберите файл...');
        });

        $(".operations-wrap .btn.blue").click(function () {
            if ($('.file-filter input').val() != '') {
                $('.operations-block').hide();
                $('.upload-block').fadeIn(400);
            }

        });
    }

    if ($('.admin_list-page').length) {
        $('.input-group.date').datepicker({
            format: "dd.mm.yyyy",
            todayBtn: "linked",
            clearBtn: true,
            language: "ru",
            autoclose: true
        }).on('changeDate', function (e) {

        });
        $('.selectpicker').selectpicker({});
        $('.checkbox').on('click', function () {
            $(this).find('input[type="checkbox"]').click();
        });

        $('.file-filter input').on('change', function () {
            let filename = $(this).val().split('\\').pop();
            if (filename)
                $(".file-filter .custom-file-label").text(filename);
            else
                $(".file-filter .custom-file-label").text('Выберите файл...');
        });
    }

});