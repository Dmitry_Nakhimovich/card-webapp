module.exports = {
  prod: {
    pug: 'prod/',
    html: 'prod/',
    js: 'prod/js/',
    css: 'prod/css/',
    img: 'prod/img/',
    fonts: 'prod/fonts/'
  },
  build: {
    pug: 'dist/',
    html: 'dist/',
    js: 'dist/js/',
    css: 'dist/css/',
    img: 'dist/img/',
    fonts: 'dist/fonts/'
  },
  src: {
    pug: 'src/pug/*.pug',
    html: 'src/*.html',
    js: 'src/js/*.js',
    jshint: 'src/js/**/*.js',
    style: 'src/style/main.css',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*',
    scripts: 'src/scripts/*.js',
    styles: 'src/style/other/*.css'
  },
  watch: {
    pug: 'src/pug/**/*.pug',
    html: 'src/**/*.html',
    js: 'src/js/**/*.js',
    style: 'src/style/**/*.css',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  clean: './dist'
};